import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import SignIn from '../views/SignIn';
import HomeTabs from './HomeTabs';
import {
  appliedOfferDetailsRoute,
  availableOfferDetailsRoute,
  HomeTabsRoute,
  SignInRoute,
} from '../global/strings';
import AvailableOfferDetails from '../views/offers/available/AvailableOfferDetails';
import AppliedOfferDetails from '../views/offers/applied/AppliedOfferDetails';

const Stack = createStackNavigator();

export default function HirefStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name={SignInRoute}
        component={SignIn}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name={HomeTabsRoute}
        component={HomeTabs}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name={availableOfferDetailsRoute}
        component={AvailableOfferDetails}
        options={{
          title: '',
        }}
      />
      <Stack.Screen
        name={appliedOfferDetailsRoute}
        component={AppliedOfferDetails}
        options={{
          title: '',
        }}
      />
    </Stack.Navigator>
  );
}
