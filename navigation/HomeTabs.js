import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import AppliedOffers from '../views/offers/applied';
import {appliedOffersRoute, availableOffersRoute} from '../global/strings';
import AvailableOffers from '../views/offers/available';
import {Icon} from 'react-native-elements';

const Tab = createBottomTabNavigator();

export default function HomeTabs() {
  return (
    <Tab.Navigator
      tabBarOptions={{
        inactiveBackgroundColor: '#2b2e30',
        activeBackgroundColor: '#2b2e30',
        activeTintColor: '#e5a832',
        inactiveTintColor: '#fff',
        allowFontScaling: true,
        style: {height: 60},
        tabStyle: {padding: 5},
      }}>
      <Tab.Screen
        name={availableOffersRoute}
        component={AvailableOffers}
        options={{
          title: 'Available offers',
          tabBarIcon: ({color}) => (
            <Icon name="receipt-long" type="material" color={color} />
          ),
        }}
      />
      <Tab.Screen
        name={appliedOffersRoute}
        component={AppliedOffers}
        options={{
          title: 'Applied offers',
          tabBarIcon: ({color}) => (
            <Icon name="event-available" type="material" color={color} />
          ),
        }}
      />
    </Tab.Navigator>
  );
}
