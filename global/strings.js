export const SignInRoute = 'sign-in';
export const HomeTabsRoute = 'home-tabs-route';
export const availableOffersRoute = 'available-offers-home';
export const appliedOffersRoute = 'applied-offers-home';
export const availableOfferDetailsRoute = 'available-offer-details-route';
export const appliedOfferDetailsRoute = 'applied-offer-details-route';
