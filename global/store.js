import rootReducer from '../reducers';
import {applyMiddleware, compose, createStore} from 'redux';
import sagaMiddleware from '../middlewares';

const store = createStore(
  rootReducer,
  compose(applyMiddleware(...[sagaMiddleware])),
);

export default store;
