import firebase from '@react-native-firebase/app';
import rrfConfig from './rrf.config';
import store from './store';
import {createFirestoreInstance} from 'redux-firestore';

const rrfProps = {
  firebase,
  config: rrfConfig,
  dispatch: store.dispatch,
  createFirestoreInstance,
};

export default rrfProps;
