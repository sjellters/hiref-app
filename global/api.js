import axios from 'axios';

const baseURL = 'https://hiref.herokuapp.com/api';

const axiosInstance = (headers) => {
  return axios.create({
    baseURL,
    mode: 'no-cors',
    headers,
  });
};

const Get = async (route, params = {}, headers = {}) => {
  try {
    const {data} = await axiosInstance(headers).get(route, {
      params,
    });
    return data;
  } catch (error) {
    throw error;
  }
};

const Post = async (route, json = {}, headers = {}) => {
  console.log('json: ' + JSON.stringify(json));
  try {
    const {data} = await axiosInstance(headers).post(route, json);
    return data;
  } catch (error) {
    throw error;
  }
};

export {Get, Post};
