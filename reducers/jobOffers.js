import {GET_JOB_OFFERS} from '../constants';

const initialState = {
  loading: false,
  error: '',
  data: {
    acceptedJobOffers: [],
    rejectedJobOffers: [],
    availableJobOffers: [],
  },
};

const jobOffersReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_JOB_OFFERS.REQUESTED:
      return {
        ...state,
        loading: true,
      };
    case GET_JOB_OFFERS.SUCCESS:
      console.log({
        ...state,
        loading: false,
        data: action.payload,
      });

      return {
        ...state,
        loading: false,
        data: action.payload,
      };
    case GET_JOB_OFFERS.ERROR:
      return {
        ...state,
        loading: false,
        data: action.payload.message,
      };
    default:
      return state;
  }
};

export default jobOffersReducer;
