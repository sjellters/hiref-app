import {all, fork} from 'redux-saga/effects';
import jobOffersSaga from './jobOffers';

function* rootSaga() {
  yield all([fork(jobOffersSaga)]);
}

export default rootSaga;
