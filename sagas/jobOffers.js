import {call, put, takeLatest} from 'redux-saga/effects';
import {GET_JOB_OFFERS} from '../constants';
import {getJobOffersError, getJobOffersSuccess} from '../actions';
import {Post} from '../global/api';

function* getJobOffers({payload}) {
  try {
    const {
      message: {result},
    } = yield call(Post, '/jobOffers/getAll/forAspirant', payload);
    yield put(getJobOffersSuccess(result));
  } catch (error) {
    const {message} = error.response;
    yield put(getJobOffersError(message));
  }
}

export default function* getJobOffersSaga() {
  yield takeLatest(GET_JOB_OFFERS.REQUESTED, getJobOffers);
}
