import React, {useState} from 'react';
import {StyleSheet, ToastAndroid, View} from 'react-native';
import {HomeTabsRoute} from '../global/strings';
import {Button, Input} from 'react-native-elements';
import auth from '@react-native-firebase/auth';
import SvgLogo from '../assets/SvgLogo';
import LinearGradient from 'react-native-linear-gradient';

export default function SignIn({navigation}) {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const signIn = () => {
    // noinspection JSUnresolvedFunction
    auth()
      .signInWithEmailAndPassword(email, password)
      .then(() => {
        navigation.navigate(HomeTabsRoute);
      })
      .catch(() => {
        ToastAndroid.show('Invalid email or password', ToastAndroid.SHORT);
      });
  };

  return (
    <View style={[styles.container]}>
      <SvgLogo style={{marginBottom: 30}} />
      <Input
        placeholder="Email"
        value={email}
        inputStyle={{color: 'white'}}
        onChangeText={(_email) => setEmail(_email)}
      />
      <Input
        placeholder="Password"
        value={password}
        onChangeText={(_password) => setPassword(_password)}
        containerStyle={{marginBottom: 30}}
        secureTextEntry={true}
        inputStyle={{color: 'white'}}
      />
      <Button
        ViewComponent={LinearGradient}
        linearGradientProps={{
          colors: ['#b77a37', '#e5a832'],
          start: {x: 0, y: 0.5},
          end: {x: 1, y: 0.5},
        }}
        containerStyle={{
          width: '100%',
          borderRadius: 15,
        }}
        title="SIGN IN"
        onPress={() => signIn()}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
    backgroundColor: '#111',
  },
});
