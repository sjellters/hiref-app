import React from 'react';
import {StyleSheet, TouchableNativeFeedback, View} from 'react-native';
import {Badge, Card, Text} from 'react-native-elements';
import {
  appliedOfferDetailsRoute,
  availableOfferDetailsRoute,
} from '../../../global/strings';

export default function OfferCard({navigation, offer, type = 'available'}) {
  const goToOfferDetails = () => {
    navigation.navigate(
      type === 'available'
        ? availableOfferDetailsRoute
        : appliedOfferDetailsRoute,
      {offer},
    );
  };

  const getParsedDate = (date_) => {
    const date = new Date(date_);
    return date.toISOString().substring(0, 10);
  };

  return (
    <TouchableNativeFeedback onPress={() => goToOfferDetails()}>
      <Card>
        <View style={[styles.card_header]}>
          <Text style={[styles.card_title]}>{offer.description.title}</Text>
          <Badge
            status="error"
            containerStyle={[styles.badge_container]}
            badgeStyle={[styles.badge]}
            textStyle={[styles.badge_text]}
            value={getParsedDate(offer.deadline)}
          />
        </View>
        <Card.Divider />
        <View>
          <Text>{offer.description.content}</Text>
        </View>
      </Card>
    </TouchableNativeFeedback>
  );
}

const styles = StyleSheet.create({
  card_header: {
    flex: 1,
    alignItems: 'flex-start',
  },
  card_title: {
    fontWeight: 'bold',
    fontSize: 20,
  },
  badge_container: {
    marginTop: 10,
    marginBottom: 10,
  },
  badge: {
    padding: 5,
  },
  badge_text: {
    fontSize: 15,
    fontWeight: 'bold',
  },
});
