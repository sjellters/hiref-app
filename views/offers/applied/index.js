import React, {useEffect} from 'react';
import {ScrollView} from 'react-native';
import OfferCard from '../components/OfferCard';
import {SafeAreaView} from 'react-native-safe-area-context';
import {useDispatch, useSelector} from 'react-redux';
import {getJobOffersRequested} from '../../../actions';

export default function AppliedOffers({navigation}) {
  const jobOffers = useSelector((state) => state.jobOffers);
  const {occupations, acceptedJobOffers, rejectedJobOffers} = useSelector(
    (state) => state.firebase.profile,
  );
  const dispatch = useDispatch();

  useEffect(() => {
    const jobOffersForAspirantArgs = {
      occupations: occupations,
      accepted: acceptedJobOffers,
      rejected: rejectedJobOffers,
    };

    dispatch(getJobOffersRequested(jobOffersForAspirantArgs));
  }, [dispatch, occupations, acceptedJobOffers, rejectedJobOffers]);

  return (
    <SafeAreaView>
      <ScrollView>
        {jobOffers.loading === false &&
          jobOffers.data.acceptedJobOffers.map((jobOffer) => (
            <OfferCard
              key={jobOffer._id}
              navigation={navigation}
              offer={jobOffer}
              type={'applied'}
            />
          ))}
      </ScrollView>
    </SafeAreaView>
  );
}
