import React from 'react';
import {ToastAndroid, View, StyleSheet, ScrollView} from 'react-native';
import {Badge, Button, Card, Text} from 'react-native-elements';
import {useDispatch, useSelector} from 'react-redux';
import {useFirebase} from 'react-redux-firebase';
import {Post} from '../../../global/api';
import {getJobOffersRequested} from '../../../actions';
import {availableOffersRoute} from '../../../global/strings';
import {SafeAreaView} from 'react-native-safe-area-context';

export default function AppliedOfferDetails({route, navigation}) {
  const {offer} = route.params;
  const firebase = useFirebase();
  const profile = useSelector((state) => state.firebase.profile);
  const {occupations, acceptedJobOffers, rejectedJobOffers} = useSelector(
    (state) => state.firebase.profile,
  );
  const user = useSelector((state) => state.firebase.auth);
  const dispatch = useDispatch();

  const getParsedDate = (date_) => {
    const date = new Date(date_);
    return date.toISOString().substring(0, 10);
  };

  const updateUserProfile = (role, jobOfferId) => {
    const _acceptedJobOffers = [...profile.acceptedJobOffers];
    _acceptedJobOffers.push(jobOfferId);

    Post(`/jobOffers/postulate/${jobOfferId}`, {
      args: {
        roles: [
          {
            applicants: [user.uid],
            description: {
              content: role.description.content,
              title: role.description.title,
            },
            quantity: role.quantity,
            remuneration: role.remuneration,
          },
        ],
      },
    })
      .then(() => {
        firebase
          .updateProfile({
            acceptedJobOffers: _acceptedJobOffers,
          })
          .then(() => {
            const jobOffersForAspirantArgs = {
              occupations: occupations,
              accepted: acceptedJobOffers,
              rejected: rejectedJobOffers,
            };
            dispatch(getJobOffersRequested(jobOffersForAspirantArgs));
            navigation.navigate(availableOffersRoute);
            ToastAndroid.show('You applied correctly', ToastAndroid.SHORT);
          })
          .catch(() => {
            ToastAndroid.show(
              'The operation cannot proceed',
              ToastAndroid.SHORT,
            );
          });
      })
      .catch(() =>
        ToastAndroid.show('The operation cannot proceed', ToastAndroid.SHORT),
      );
  };

  const RolDetail = ({role, jobOfferId}) => {
    return (
      <Card>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <Text h3 style={{marginBottom: 10}}>
            {role.description.title}
          </Text>
          <Badge
            status="warning"
            containerStyle={[styles.badge_container]}
            badgeStyle={[styles.badge]}
            textStyle={[styles.badge_text]}
            value={`${role.applicants.length}/${role.quantity}`}
          />
        </View>
        <Card.Divider />
        <Text style={{marginBottom: 10}}>{role.description.content}</Text>
        <Text h5 style={{marginBottom: 10, fontWeight: 'bold'}}>
          Remuneration: {role.remuneration}
        </Text>
        <Button
          title="LEAVE"
          type={'outline'}
          onPress={() => updateUserProfile(role, jobOfferId)}
        />
      </Card>
    );
  };

  return (
    <SafeAreaView>
      <ScrollView>
        <View style={{padding: 15}}>
          <View style={{alignItems: 'flex-start'}}>
            <Text h2 style={{marginBottom: 5}}>
              {offer.description.title}
            </Text>
            <Badge
              status="error"
              containerStyle={[styles.badge_container]}
              badgeStyle={[styles.badge]}
              textStyle={[styles.badge_text]}
              value={getParsedDate(offer.deadline)}
            />
          </View>
          <Text style={{marginBottom: 10}}>{offer.description.content}</Text>
        </View>
        {offer.roles.map((role) => (
          <RolDetail
            key={role.description.title}
            role={role}
            jobOfferId={offer._id}
          />
        ))}
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  badge_container: {
    marginTop: 10,
    marginBottom: 10,
  },
  badge: {
    padding: 5,
  },
  badge_text: {
    fontSize: 15,
    fontWeight: 'bold',
  },
});
