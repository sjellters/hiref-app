import {GET_JOB_OFFERS} from '../constants';

export const getJobOffersRequested = (jobOffersForAspirantArgs) => ({
  type: GET_JOB_OFFERS.REQUESTED,
  payload: {args: jobOffersForAspirantArgs},
});

export const getJobOffersSuccess = (jobOffers) => ({
  type: GET_JOB_OFFERS.SUCCESS,
  payload: jobOffers,
});

export const getJobOffersError = (message) => ({
  type: GET_JOB_OFFERS.ERROR,
  payload: {message},
});
