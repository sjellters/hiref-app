/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import 'react-native-gesture-handler';
import React from 'react';
import {Provider} from 'react-redux';
import store from './global/store';
import {ReactReduxFirebaseProvider} from 'react-redux-firebase';
import '@react-native-firebase/auth';
import '@react-native-firebase/firestore';
import rrfProps from './global/rrf.props';
import HirefStack from './navigation/HirefStack';
import {NavigationContainer} from '@react-navigation/native';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import rootSaga from './sagas';
import sagaMiddleware from './middlewares';

sagaMiddleware.run(rootSaga);

const App: () => React$Node = () => {
  return (
    <Provider store={store}>
      <ReactReduxFirebaseProvider {...rrfProps}>
        <SafeAreaProvider>
          <NavigationContainer>
            <HirefStack />
          </NavigationContainer>
        </SafeAreaProvider>
      </ReactReduxFirebaseProvider>
    </Provider>
  );
};

export default App;
